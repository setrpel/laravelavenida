<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert([
            'nome' => 'Sem Saladas',
            'descricao' => 'Pratos acompanhamentos de saladas',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);

        DB::table('tipos')->insert([
            'nome' => 'Sem Lactose',
            'descricao' => 'Pratos sem leite animal',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('tipos')->insert([
            'nome' => 'Sem Restrições',
            'descricao' => 'Pratos sem restrições de saladas ou acompanhamentos',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('tipos')->insert([
            'nome' => 'Vegano',
            'descricao' => 'Pratos sem derivações de animais',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('tipos')->insert([
            'nome' => 'Vegetariano',
            'descricao' => 'Pratos sem carnes',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
       
        
       
        
    }
}
