<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome',100);
            $table->string('endereco',100);
            $table->string('descricao',100);
            $table->integer('prato_id')->unsigned();
            $table->string('email',100);
            $table->string('telefone',100);
            $table->decimal('preco',10,2);
            $table->integer('tipo_id');
            $table->string('foto',100);
            $table->timestamps();

            $table->foreign('prato_id')->references('id')->on('pratos');
           
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
