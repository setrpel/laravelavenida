<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pratos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome',100);
            $table->string('foto',100);
            $table->decimal('preco',10,2);
            $table->string('descricao',100);
            $table->integer('tipo_id')->unsigned();
            $table->timestamps();
            $table->foreign('tipo_id')->references('id')->on('tipos');
           
            
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pratos');
    }
}
