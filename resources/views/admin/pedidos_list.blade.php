@extends('adminlte::page')

@section('title', 'Lista de Pedidos')

@section('content_header')
    
@stop

@section('content')

@if (session('status'))
   <div class="alert alert-success">
      {{ session('status') }}
   </div> 
@endif

<table class="table table-striped">
  <thead>
    <tr>
    <th>Nº</th>
      <th>Nome</th>
      <th>Endereço</th>
      <th>Telefone</th>
      <th>Prato</th>
      <th>Email</th>     
      <th>Valor</th>     
      <th>Data</th>
      <th>Enviar</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pedidos as $p)
      <tr>
        <td> {{$p->id}} </td>
        <td> {{$p->nome}} </td>
        <td> {{$p->endereco}} </td>
        <td> {{$p->telefone}} </td>
        <td> {{$p->prato}} </td>
        <td> {{$p->email}} </td>
        <td> R$: {{number_format($p->preco, 2, ',', '.')}} </td>
        <td> {{($p->created_at)}} </td>
        
        
        <td><a href="{{ route('pedidos.email', $p->id) }}" 
          class="btn btn-primary btn-sm" role="button">Email</a>&nbsp;</td>
        
         
            
               
                  
                   {{csrf_field()}}
                  
            </form>
        </td>
        @if ($loop->iteration == $loop->count)
        <tr><td colspan=8><p>Soma dos Pedidos R$: 
                               {{number_format($soma, 2, ',', '.')}}</p>
                               <p>Nº de Pedidos Cadastrados: {{$numPedidos}} </p>                             
                              </td></tr>
        @endif        
    @empty
      <tr><td colspan=8> Não há pedidos cadastrados ou 
                         para o filtro informado </td></tr>
    @endforelse

  </tbody>
</table>  

{{ $pedidos->links() }}
@stop

