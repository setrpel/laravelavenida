<<div class="container-fluid">
 <h1 style="text-align: center">Delivery Avenida</h1>
 <h3 style="text-align: center">Relatório de Pedidos</h3>
<table class="table table-bordered table-sm">
 <thead class="thead-light">
 <tr>
 <th>Data:</th><th>Pedido N°:   </th><th>Nome:</th><th>Cód. Prato:</th><th>Valor:</th>
 </tr>
 </thead>
 

 <tbody>

    @foreach ($pedidos as $p)
    <tr>
    <td> {{date_format($p->created_at, 'd/m/Y H:m')}} </td>
      <td> {{ $p->id }} </td>
      <td> {{ $p->nome }} </td>
      <td> {{ $p->prato_id}} </td>
      <td> R$: {{number_format($p->preco, 2, ',', '.')}}</td>

      </tbody>          
               
      {{csrf_field()}}
                  
                  </form>
              </td>
              @if ($loop->iteration == $loop->count)
              <tr><td colspan=8><p>Soma dos Pedidos R$: 
                                     {{number_format($soma, 2, ',', '.')}}
                                     <p>Nº de Pedidos Cadastrados: {{$numPedidos}} </p>                             
                                    </td></tr>
              @endif        
         
       
                                     </tr>

@endforeach

</tbody>
</table>