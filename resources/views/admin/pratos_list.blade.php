@extends('adminlte::page')

@section('title', 'Cadastro de Pratos')

@section('content_header')
    <h1>Cadastro de Pratos
    <a href="{{ route('pratos.create') }}" class="btn btn-primary pull-right"
       role="button">Novo</a>
    </h1>
@stop

@section('content')

@if (session('status'))
   <div class="alert alert-success">
      {{ session('status') }}
   </div> 
@endif

<table class="table table-striped">
  <thead>
    <tr>
      <th>Nome</th>
      <th>Tipo</th>
      <th>Descrição</th>
      <th>Preço R$</th>     
      <th>Foto</th>
      <th>Ações</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pratos as $p)
      <tr>
        <td> {{$p->nome}} </td>
        <td> {{$p->tipo->nome}} </td>
        <td> {{$p->descricao}} </td>
        <td> {{number_format($p->preco, 2, ',', '.')}} </td>
        <td>
          @if (Storage::exists($p->foto))
            <img src="{{url('storage/'.$p->foto)}}" 
                 style="width: 100px; height: 60px" alt="Foto">
          @else
            <img src="{{url('storage/fotos/semfoto.png')}}" 
                 style="width: 100px; height: 60px" alt="Foto">
         @endif
        </td>
        <td> 
            <a href="{{route('pratos.edit', $p->id)}}" 
                class="btn btn-warning btn-sm" title="Alterar"
                role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
            <form style="display: inline-block"
                  method="post"
                  action="{{route('pratos.destroy', $p->id)}}"
                  onsubmit="return confirm('Confirma Exclusão?')">
                   {{method_field('delete')}}
                   {{csrf_field()}}
                  <button type="submit" title="Excluir"
                          class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
            </form>
        </td>
        @if ($loop->iteration == $loop->count)
             <tr><td colspan=8>Total dos Pratos cadastrados: {{$numPratos}}                              
                              </td></tr>
        @endif        
    @empty
      <tr><td colspan=8> Não há pratos cadastrados ou 
                         para o filtro informado </td></tr>
    @endforelse

  </tbody>
</table>  

{{ $pratos->links() }}
@stop

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection

