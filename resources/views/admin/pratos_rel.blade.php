<<div class="container-fluid">
 <h1 style="text-align: center">Cardápio Delivery Avenida</h1>
 <h5 style="text-align: center">Lista de Pratos Cadastrados</h5>
<table class="table table-bordered table-sm">
 <thead class="thead-light">
 <tr>
 <th>Numero</th><th>Nome</th><th>Tipo de Prato</th><th>Descrição</th><th>Preço</th><th>Foto</th>
 </tr>
 </thead>
 

 <tbody>

    @foreach ($pratos as $prato)
    <tr>
      <td> {{ $prato->id }} </td>
      <td> {{ $prato->nome }} </td>
      <td> {{ $prato->tipo->nome }} </td>
      <td> {{ $prato->descricao }} </td>
      <td> R$: {{number_format($prato->preco, 2, ',', '.')}} </td>
      <td> <img src='storage/{{ $prato->foto }}' style='width: 120px; height: 80px;'> </td>
      
     
    </tr>

    @endforeach

  </tbody>
</table>

