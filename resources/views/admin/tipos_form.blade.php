@extends('adminlte::page')

@section('title', 'Cadastro de Tipos')

@section('content_header')
 
    @if ($acao==1)
       <h2>Inclusão de Tipos
    @elseif ($acao ==2)
       <h2>Alteração de Tipos
    @endif          

    <a href="{{ route('tipos.index') }}" class="btn btn-primary pull-right"
       role="button">Voltar</a>
    </h2>

@endsection

@section('content')

   <div class="container-fluid">

    @if ($acao==1)
      <form method="POST" action="{{ route('tipos.store') }}" enctype="multipart/form-data">
    @elseif ($acao==2)
      <form method="POST" action="{{route('tipos.update', $reg->id)}}" enctype="multipart/form-data">
      {!! method_field('put') !!}
    @endif          
    {{ csrf_field() }}

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Nome do Tipo de Prato</label>
          <input type="text" id="nome" name="nome" required 
                 value="{{$reg->nome or old('nome')}}"
                 class="form-control">
        </div>
      </div>

      
    </div>              

    
      
      

    <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="ano">Descrição</label>
            <input type="text" id="descricao" name="descricao" required 
                   value="{{$reg->descricao or old('descricao')}}"
                   class="form-control">
          </div>
        </div>
      
       

        
    </div>

    <input type="submit" value="Enviar" class="btn btn-success">
    <input type="reset" value="Limpar" class="btn btn-warning">

    </form>
  </div>

@endsection

@section('js')
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="/js/jquery.mask.min.js"></script>

  
@endsection