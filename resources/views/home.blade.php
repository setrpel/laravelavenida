@extends('site')

@section('conteudo')

<div class="row">
  <div class="col-sm-10">
     
  
  <div class="container text-center ">
  <font color="White">
     <H1> Vamos Comer!</h1>
	 </font>
 </div>
 

  
<div class="container">

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<div class="row">

		<nav class="navbar navbar-default"  style="margin-top: 30px; margin-bottom: 30px;">
		  <div class="container-center">
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		     <ul class="nav navbar-nav navbar-center">
		        <form method="POST" action="{{ route('homeBusca') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
		         	<div class="form-group">
		            	<input name="nome" type="text" class="form-control" placeholder="Prato ou Acompanhamento">
		        	</div>
		          <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> </button>
		        </form>
		      </ul>
		    </div>
		  </div>
		</nav>
	</div>

	
		     
			 
		        
		      
		    

  <div class="container">

	<div class="row" >
		@forelse($linhas as $linha)
		
		  <div class="col-xs-6 col-md-3 ">
		  
		    <div class="thumbnail text-center"  style='width: 240px; height: 240px;'>
			
        <td> <img src='storage/{{ $linha->foto }}' style='width: 30%;'> </td>
		      <div class="caption">
		        <h3>{{$linha->nome}}</h3>
		        <p>{{$linha->descricao}}</p>
				<td> R$: {{number_format($linha->preco, 2, ',', '.')}} </td>
		        <p><a href="/home_detalhes/{{$linha->id}}" class="link" role="link">Detalhes</a></p>
		      </div>
		    </div>
		  </div>
		
		@empty
		<tr><td colspan=8> Não há pratos cadastrados ou filtro da pesquisa não encontrou registros </td></tr>
		@endforelse
		</table>
			
	</div>
</div>


@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif