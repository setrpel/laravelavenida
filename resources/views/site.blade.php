<!DOCTYPE html>
<html lang="pt">
<style>
            
            html, body {
                background-color: #770f0f;
                color: #770f0f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 0vh;
                margin: 0;
            }        
            </style>
<head>

  <title>Delivery Avenida</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>



<nav class="navbar navbar-default" style="margin-top: 30px; margin-bottom: 30px;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('home') }}">Delivery Avenida</a>
      
    </div>
    <div class="navbar-header">
    <a class="navbar-brand" href="/admin/relpratos">Cardápio</a>
    </div>
    <ul class="navbar-right">
    
                    @auth
                        <a class="navbar-brand"href="{{ url('/admin/pratos') }}">{{ Auth::user()->name }}</a>
                    @else
                        <a class="navbar-brand"href="{{ route('login') }}">Login 
                        <a class="navbar-brand"href="{{ route('register') }}">Registro</a>
                    @endauth
                   
      
      
      </ul>
    
  </div>
</nav>
  
<div class="container">

@yield('conteudo')  

</div>

</body>
</html>