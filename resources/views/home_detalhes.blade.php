@extends('site')
@section('conteudo')
@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif



<div class="container">
	<div class="row">
		
			
	
				
			
			<div class="thumbnail">			
			<div class="modal-body">
				<div class="row">
				<form method="POST" action="{{ route('homePedir') }}">
				{{ csrf_field() }}
			
				<div class="col-md-6">
				<h1 class="modal-title" id="myModalLabel">Prato</h1>
								<H3 >{{$detalhes->nome}}</H3>
								<input type="hidden" name="prato_id" value="{{$detalhes->id}}">
							</div>

				<div class="col-md-6">
								<label for="email">Decrição / Acompanhamentos: </label>
								<p >{{$detalhes->descricao}}</p>
								<input type="hidden" name="descricao" value="{{$detalhes->descricao}}"> 
							</div>
           
			<div class="col-md-6">
								<label for="email">Tipo de Alimento: </label>
								<p >{{$detalhes->tipo->nome}}</p>
								<input type="hidden" name="tipo_id" value="{{$detalhes->tipo_id}}">
							</div>
					<div class="col-md-6">
                    @if (Storage::exists($detalhes->foto))
				<img src="{{url('storage/'.$detalhes->foto)}}" style='width: 120px; height: 80px;' alt="Foto do Prato" >
				<input type="hidden" class="form-control" id="foto" name="foto" value="{{$detalhes->foto}}">
			@else
				<img src="{{url('storage/'.$detalhes->foto)}}" style='width: 120px; height: 80px;' alt="Sem Foto">
				<input type="hidden" class="form-control" id="foto" name="foto"value="{{$detalhes->tipo_id}}">
			@endif
			<div >			
			<H1><td> R$: {{number_format($detalhes->preco, 2, ',', '.')}} </td></H1>
			<input type="hidden" id="preco" name="preco"value="{{$detalhes->preco}}">
			</div>

					</div>
					<div class="col-md-6">
					
									
							<div class="form-group">
								<label for="nome">Nome:</label>
								<input name="nome" type="text" class="form-control" id="nome" placeholder="Digite seu nome" 
								pattern="[A-Za-z\d]+ [A-Za-z\d](?:[A-Za-z\d]| (?! |$)){0,29}" title="
								•Letras&#10;• Não pode haver Números&#10;•Mínimo 10 e no Máximo 30 caracteres&#10;• Obrigatório nome e sobrenome&#10;• Não pode haver espaço duplo&#10;• Não pode haver espaço no início e no fim" required>
							</div>
							<div class="form-group">
								<label for="nome">Endereco:</label>
								<input name="endereco" type="text" class="form-control" id="endereco" placeholder="Digite seu endereço" required>
								
							</div>

                            <div class="form-group">
								<label for="telefone">Telefone:</label>
								<input name="telefone" type="number" class="form-control" id="telefone" placeholder="Digite seu telefone" required>
							</div>
							<div class="form-group">
								<label for="email">E-mail:</label>
								<input name="email" type="email" class="form-control" id="email" placeholder="E-mail" required>
							</div>
							
								<input type="submit" value="Comprar" class="btn btn-success">	
								<div class="modal-footer">
				
				<p><a href="/home" class="btn btn-default" role="button">Cancelar</a></p>
				</div>				
							</form>
					</div>
				</div>
			
			
                </div>
	</div>
</div>
	


 	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-latest.min.js"></script>
	<script src="/js/jquery.mask.min.js"></script>
	<script>
  		
  		});
	</script>
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif