<h2>Pedido Realizado com Sucesso!</h2>
<h3>Em cerca de 20 minutos estaremos entregando seu pedido!</h3>
<h4>Local de entrega:{{$pedido->endereco}} </h4>
<h4>Detalhes do Prato: {{$pedido->descricao}} </h4>
<h4> Código do Prato: {{$pedido->prato_id}}</h4>
<h4> Valor: {{number_format($pedido->preco, 2, ',', '.')}}</h4>
<img src="{{$message->embed(public_path('storage/'.$pedido->foto))}}"
style="width: 300px; heigth: 250px" alt="Foto do Prato">