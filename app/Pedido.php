<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Pedido extends Model
{

    protected $fillable = ['nome', 'prato_id', 'descricao', 'endereco','email', 'telefone', 'preco', 'tipo_id','foto'];

	public function pedido() {
        return $this->belongsTo('App\Prato');

    }
    
  /*  public function getModeloAttribute($value) {
        return strtoupper($value);
    }
    
    public function setPrecoAttribute($value) {
        $novo1 = str_replace('.', '', $value);    // retira o ponto
        $novo2 = str_replace(',', '.', $novo1);   // substitui a , por .
        $this->attributes['preco'] = $novo2;


}*/

}