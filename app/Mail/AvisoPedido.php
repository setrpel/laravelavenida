<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvisoPedido extends Mailable
{
    use Queueable, SerializesModels;

    private $pedido;
    

    
    public function __construct($pedido)
    {
    $this->pedido = $pedido;    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.pedido', ['pedido'=>$this->pedido]);
    }
}
