<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Prato;
use App\Tipo;
use App\Pedido;

class HomeController extends Controller
{
    
   
      public function index()
      {
         // obtém os registros cadastrados na tabela pratos
         $linhas = Prato::orderBy('nome')->get();
  
         return view('home', ['linhas' => $linhas]);
      }
  
      public function detalhes($id) {
         // posiciona no registro a ser alterado e obtém seus dados
         $dados = Prato::find($id);
         return view('home_detalhes', ['detalhes' => $dados]);
     }
         
     public function homePedir(Request $request){
         
        
        $this->validate($request, [
             'nome' => 'min:10|max:40',
             'email'=> 'required|email|max:30'
       
            
        ]);        
         $dados = $request->all();
         
         $inc = Pedido::create($dados);
         
 
         if ($inc) {
             return redirect()->route('home')
                    ->with('status','Pedido enviado com sucesso! Em breve você receberá um email informando o tempo para entrega. Obrigado!');   
                   
                     }
         else {
 
             return redirect()->route('home')
              ->with('status','Seu pedido já foi computado anteriormente!');  
             }  
     }
 
public function homeBusca(Request $request) {
       
       
    $nome = $request->nome;
    $pesq = DB::table('pratos')
    ->where('nome', 'like', '%'.$nome.'%' )->orWhere ('descricao', 'like', '%'.$nome.'%')
    ->get();
    $count = DB::table('pratos')
    ->where('nome', 'like', '%'.$nome.'%' )->orWhere ('descricao', 'like', '%'.$nome.'%')
    ->count();
    if($count == 0){
        return redirect()->route('home.index')
           ->with('status', 'Não há Pratos ou ingredientes com a palavra chave informada');
    }
    
 else {
    return view('home', ['linhas' => $pesq]);
}
 }

} 

