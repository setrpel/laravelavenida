<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prato;
class JSController extends Controller
{
    
    public function index()
    {
        $pratos = Prato::orderBy('nome')->get();
        return response()
        ->json($pratos, 200, [], JSON_PRETTY_PRINT);
    }

       
    public function store(Request $request)
    {
        $dados = $request->all();
        $inc = Prato::create($dados);
        if($inc){

            return response()->json([$inc], 201);
        }else{
            return response()->json(['error'=>'error_insert'],500);

        }
    }

  
    public function show($id)
    {
        $reg = Prato::find($id);

        if($reg){
            return response()
            ->json($reg, 200,[], JSON_PRETTY_PRINT);
        }else {
            return response()
            ->json(['error'=> 'not_found'],404);
        
        }
    }

    
   

   
    public function update(Request $request, $id)
    {
        $reg = Prato::find($id);

        if($reg){
 
 $dados = $request->all();
 $alt = $reg->update($dados);
 if($alt){
 
            return response()
            ->json($reg, 200,[], JSON_PRETTY_PRINT);
        }else {
            return response()
            ->json(['error'=> 'not_update'],500);
     }
 }else {
         return response()
         ->json(['error'=> 'not_found'],404);
  }
    }

  
    public function destroy($id)
    {
        $reg = Prato::find($id);

        if($reg){
 
 if($reg->delete()){

    
            return response()
            ->json(['msg'=>'Ok! Excluíd'], 200);
        }else {
            return response()
            ->json(['error'=> 'not_destroy'],500);
     }
 }else {
         return response()
         ->json(['error'=> 'not_found'],404);
  }
    }
    }

