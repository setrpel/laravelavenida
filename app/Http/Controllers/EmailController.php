<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\EmailController;
use App\Mail\Aviso;

class EmailController extends Controller
{
    public function enviaEmail() {
        $destinatario = "setrpel@gmail.com";
        Mail::to($destinatario)->send(new Aviso());
    }
}
