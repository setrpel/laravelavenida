<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Prato;
use App\Tipo;

class PratoController extends Controller
{

//  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
//        $dados = Prato::all();
        $dados = Prato::paginate(3);

            // obtém a soma do campo preço

        $numPratos = Prato::count('id');    // obtém o número de registros

        return view('admin.pratos_list', ['pratos'=>$dados, 
                                          
                                          'numPratos'=>$numPratos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = Tipo::orderBy('nome')->get();

        

        return view('admin.pratos_form', ['tipos' => $tipos, 'acao' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // obtém todos os campos vindos do form
        $dados = $request->all();

        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            $path = $request->file('foto')->store('fotos');
            
            $dados['foto'] = $path;
        }

        $inc = Prato::create($dados);

        if ($inc) {
            return redirect()->route('pratos.index')
                 ->with('status', $request->nome . ' inserido com sucesso');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Prato::find($id);

        $tipos = Tipo::orderBy('nome')->get();

               
        return view('admin.pratos_form', ['reg' => $reg, 'tipos' => $tipos, 
                                          'acao' => 2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Prato::find($id);

        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {

            if (Storage::exists($reg->foto)) {
                Storage::delete($reg->foto);
            }

            $path = $request->file('foto')->store('fotos');
            
            $dados['foto'] = $path;
        }

        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('pratos.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prato = Prato::find($id);
        if ($prato->delete()) {
            return redirect()->route('pratos.index')
                            ->with('status', $prato->nome . ' Excluído!');
        }
    }

    public function grafico() {
        $pratos = Prato::selectRaw('tipos.nome as tipo, count(*) as num')
                          ->join('tipos', 'pratos.tipo_id', '=', 'tipos.id')
                          ->groupBy('tipo')
                          ->get();         
    
        return view('admin.pratos_graf', ['pratos' => $pratos]);
      }
    
    
    
    
    
}

