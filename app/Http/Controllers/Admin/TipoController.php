<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Tipo;
use App\Prato;

class TipoController extends Controller
{

//    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

//        $dados = Tipo::all();
        $dados = Tipo::paginate(3);

             return view('admin.tipos_list', ['tipos'=>$dados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       

        return view('admin.tipos_form', ['acao'=>1]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // obtém todos os campos vindos do form
        $dados = $request->all();

        
        

        $inc = Tipo::create($dados);

        if ($inc) {
            return redirect()->route('tipos.index')
                 ->with('status', $request->nome . ' inserido com sucesso');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Tipo::find($id);

       

        return view('admin.tipos_form', ['reg' => $reg, 'acao' => 2]);
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Tipo::find($id);

        // se o usuário informou a foto e a imagem foi corretamente enviada
       
        

        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('tipos.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipos = Tipo::find($id);
        if ($tipos->delete()) {
            return redirect()->route('tipos.index')
                            ->with('status', $tipos->nome . ' Excluído!');
        }
    }

   
    
    
    
   
}


    

    


