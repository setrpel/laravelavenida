<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\Prato;
use App\Tipo;
use App\Pedido;
use App\Mail\AvisoPedido;

class PedidoController extends Controller
{

//    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                
      
      $dados = DB::table('pedidos')
      ->join('pratos', 'pedidos.prato_id', '=', 'pratos.id')
      ->select ('pedidos.created_at',
      'pedidos.preco', 'pedidos.id', 'pedidos.nome', 'pedidos.endereco', 
      'pedidos.telefone','pedidos.foto',
  'pedidos.email','pratos.nome as prato')
  ->orderBy('created_at', 'desc')-> paginate(3);


        /*$sql = (" SELECT DATE_FORMAT(pedidos.created_at, '%d/%m/%Y %H:%m:%s ')as data, 
        pedidos.preco, pedidos.id, 
        pedidos.nome, pedidos.endereco, pedidos.telefone,pedidos.foto,pedidos.email,
        pratos.nome as prato from pedidos, pratos WHERE pedidos.prato_id = pratos.id 
        ORDER BY pedidos.created_at DESC");
        $pedidos = DB::select($sql);*/
        
       
       
        
        $soma = Pedido::sum('preco');
        $numPedidos = Pedido::count('id');    // obtém o número de registros

        return view('admin.pedidos_list', ['pedidos'=>$dados, 
                                          'soma'=>$soma,
                                          'numPedidos'=>$numPedidos]);
       
    }

    
    public function store(Request $request)
    { 
                
        // obtém todos os campos vindos do form
        $dados = $request->all();

        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            $path = $request->file('foto')->store('fotos');
            
            $dados['foto'] = $path;
        }

        $inc = Pedido::create($dados);

        if ($inc) {
            
            return redirect()->route('pedidos.index')
                 ->with('status', $request->nome . ' inserido com sucesso');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }
   
    public function email($id){
        $reg = Pedido::find($id);
        $email = $reg['email'];
        Mail::to($email)->send(new AvisoPedido($reg));
    
            return redirect()->route('pedidos.index')
                   ->with('status', 'Ok! Email enviado com Sucesso!');
    }


   public function grafico() {
    $sql = "select pratos.nome, count(pedidos.prato_id) as total from pratos,
    pedidos where pratos.id = pedidos.prato_id group by pratos.nome order by 2 desc, 1";
    $totais = DB::select($sql);
        
            return view('admin.pratos_graf', ['totais' => $totais]);
        }
    
        public function relatorio() {

            $pratos = Prato::orderBy('nome')->get();
    
            
    return \PDF::loadView('admin.pratos_rel', ['pratos'=>$pratos])->stream();
            }

            public function relpedidos() {
                $soma = Pedido::sum('preco');
                $numPedidos = Pedido::count('id'); 

                $pedidos = Pedido:: orderBy('created_at', 'desc')->get();
                return \PDF::loadView('admin.pedidos_rel', ['pedidos'=>$pedidos,'soma'=>$soma,
                'numPedidos'=>$numPedidos])
                             ->stream();

                             
       
            }
    

}
    



