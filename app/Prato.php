<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prato extends Model
{

    // identifica os campos que serão inseridos, ou seja, que estamos
    // dando permissão de serem atualizados na store
    protected $fillable = ['nome', 'tipo_id', 'descricao', 'preco', 'foto'];
/*
    public function tipo() {
        return $this->belongsTo('App\Tipo');
    }

    public function getModeloAttribute($value) {
        return strtoupper($value);
    }
*/
        // retira a máscara com "." e "," antes da inserção 
  /*  public function setPrecoAttribute($value) {
        $novo1 = str_replace('.', '', $value);    // retira o ponto
        $novo2 = str_replace(',', '.', $novo1);   // substitui a , por .
        $this->attributes['preco'] = $novo2;
    

}*/
}