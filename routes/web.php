<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//site

Route::post('homeBusca','homeController@homeBusca')->name('homeBusca');
Route::post('homePedir','homeController@homePedir')->name('homePedir');
Route::get("/", "homeController@index")->name('home.index');
Route::get('home_detalhes/{id}', 'homeController@detalhes');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', function() {
    return view('admin.index');
})->middleware('auth');

Route::prefix('admin')->group(function () {
    
    Route::resource('pratos', 'Admin\PratoController')->middleware('auth');
    Route::resource('pedidos', 'Admin\PedidoController')->middleware('auth');
    Route::resource('tipos', 'Admin\TipoController')->middleware('auth');
    Route::get('/grafpratos', 'Admin\PedidoController@grafico')->middleware('auth');
    Route::get('pedidos_email/{id}', 'Admin\PedidoController@email')
->name('pedidos.email')->middleware('auth');
Route::get('/relpratos', 'Admin\PedidoController@relatorio');
Route::get('/relpedidos', 'Admin\PedidoController@relpedidos');
    
    
    
});

Auth::routes();





